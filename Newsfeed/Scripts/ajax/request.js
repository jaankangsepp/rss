﻿$(document).ready(function () {
    var skip = 10;

    $(window).scroll(function () {
        if ($(window).scrollTop() >= ($(document).height() - $(window).height() - 2)) {
            setTimeout(getNews, 1000);
        }
    });

    function getNews() {
        $.ajax({
            type: "GET",
            url: "http://localhost:54114/Home/Index",
            data: { take: 5, skip: skip },
            contentType: 'application/json',
            success: function (data) {
                $.each(data, function (index, value) {
                    $("#list").append("<li>" + this['title'] + "</li>");
                })
            }
        })
        skip += 5;
        $(window).scrollTop($(document).height() - $(window).height() - 4);
    }
});