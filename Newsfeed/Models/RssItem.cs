﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newsfeed.Models
{
    public class RssItem
    {
        public int RssItemId { get; set; }
        public String link { get; set; }
        public String category { get; set; }
        public String title { get; set; }
        public String description { get; set; }
        public String pubdate { get; set; }

        public RssItem()
        {

        }

        public RssItem(String link,String category,String title,String description,String pubdate)
        {
            this.link = link;
            this.category = category;
            this.title = title;
            this.description = description;
            this.pubdate = pubdate;
        }
    }
}