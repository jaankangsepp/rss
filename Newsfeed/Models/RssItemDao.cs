﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Newsfeed.Models
{
    public class RssItemDao
    {
        protected DbContext dbContext { get; set; }
        public DbSet<RssItem> dbSet { get; set; }

        public RssItemDao()
        {
            dbContext = new NewsFeedContext();
            dbSet = dbContext.Set<RssItem>();
        }

        public IQueryable<RssItem> GetAll()
        {
            return dbSet;
        }

        public List<RssItem> Decode(IQueryable<RssItem> dbSet)
        {
            List<RssItem> results = new List<RssItem>();
            foreach (RssItem item in dbSet)
            {
                results.Add(new RssItem(
                HttpUtility.HtmlDecode(item.link),
                HttpUtility.HtmlDecode(item.category),
                HttpUtility.HtmlDecode(item.title),
                HttpUtility.HtmlDecode(item.description),
                HttpUtility.HtmlDecode(item.pubdate)
                ));
            }
            return results;
        }

        public void Add(RssItem entity)
        {
            DbEntityEntry dbEntityEntry = dbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                dbSet.Add(entity);
            }
        }

        public void clearDbSet()
        {
            if(GetAll().Any())
            dbSet.RemoveRange(GetAll().ToList());
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }

    }
}