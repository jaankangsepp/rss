﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Newsfeed.Models
{
    public class NewsFeedContext:DbContext
    {
        public DbSet<RssItem> Items { get; set; }
        public NewsFeedContext():base("NewsFeedContext")
        {

        }
    }
}