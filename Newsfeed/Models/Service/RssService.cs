﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;

namespace Newsfeed.Models.Service
{
    public class RssService
    {
        private RssItemDao rssDao;
        public RssService()
        {
            rssDao = new RssItemDao();
        }

        public List<RssItem> getAll()
        {
            if (rssDao.GetAll().Any())
                return rssDao.Decode(rssDao.GetAll());
            else
                return null;
        }

        public void getAndPersistRss()
        {
            rssDao.clearDbSet();
            rssDao.Save();
            string url = "http://err.ee/rss";
            List<RssItem> collection = MakeHttpRequest(url);
            foreach (RssItem item in collection)
            {
                rssDao.Add(item);
            }
            rssDao.Save();
        }

        private List<RssItem> MakeHttpRequest(string request)
        {
            Uri u = new Uri(request);
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(u);
            myReq.KeepAlive = false;

            using (HttpWebResponse myHttpWebResponse = myReq.GetResponse() as HttpWebResponse)
            {
                Stream response = myHttpWebResponse.GetResponseStream();
                StreamReader readStream = new StreamReader(response, Encoding.UTF8);
                string responseString = readStream.ReadToEnd();
                XmlDocument xmlD = new XmlDocument();
                xmlD.LoadXml(responseString);
                string xpath = "rss/channel/item";
                var nodes = xmlD.SelectNodes(xpath);

                List<RssItem> results = new List<RssItem>();
                string temp;
                foreach (XmlNode node in nodes)
                {
                    results.Add(new RssItem(
                        HttpUtility.HtmlEncode(node.SelectSingleNode("link").InnerText),
                        HttpUtility.HtmlEncode(node.SelectSingleNode("category").InnerText),
                        HttpUtility.HtmlEncode(node.SelectSingleNode("title").InnerText),
                        HttpUtility.HtmlEncode(node.SelectSingleNode("description").InnerText),
                        HttpUtility.HtmlEncode(node.SelectSingleNode("pubDate").InnerText)
                        ));
                }

                return results;
            }
        }
    }
}