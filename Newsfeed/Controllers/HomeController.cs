﻿using Newsfeed.Models;
using Newsfeed.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Newsfeed.Controllers
{
    public class HomeController : Controller
    {
        private RssService service = new RssService();

        // GET: Home
        [OutputCache(Duration = 300, VaryByParam = "skip")]
        public ActionResult Index(int? take, int? skip)
        {
            if (take == null || skip == null) {
                service.getAndPersistRss();
                take = 10;
                skip = 0;
                return View(service.getAll().Skip(skip.Value).Take(take.Value));
            }else {
                return Json(service.getAll().Skip(skip.Value).Take(take.Value), JsonRequestBehavior.AllowGet);
            }
        }
    }
}